from django import forms
from .models import Profile, Job, Category, Location, Review


class EditProfileForm(forms.Form):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        profile = Profile.objects.get(user=user)

        super(EditProfileForm, self).__init__(*args, **kwargs)

        self.fields['first_name'] = forms.CharField(required=True,
                                                    widget=forms.TextInput(
                                                        attrs={'class': 'inputfield',
                                                               'style': 'position: absolute; left: 410px;', }),
                                                    max_length=100,
                                                    initial=user.first_name,
                                                    label="First Name: ")

        self.fields['last_name'] = forms.CharField(required=True,
                                                   widget=forms.TextInput(
                                                       attrs={'class': 'inputfield',
                                                              'style': 'position: absolute; left: 410px;', }),
                                                   max_length=100,
                                                   initial=user.last_name,
                                                   label="Last Name: ")

        self.fields['email'] = forms.CharField(required=True,
                                               widget=forms.TextInput(
                                                   attrs={'class': 'inputfield',
                                                          'style': 'position: absolute; left: 410px;', }),
                                               max_length=100,
                                               initial=user.email,
                                               label="Email")

        self.fields['old_password'] = forms.CharField(required=True,
                                                      widget=forms.PasswordInput(
                                                          attrs={'class': 'inputfield',
                                                                 'style': 'position: absolute; left: 410px;', }),
                                                      label="Current Password: ")

        self.fields['new_password'] = forms.CharField(required=False,
                                                      widget=forms.PasswordInput(
                                                          attrs={'class': 'inputfield',
                                                                 'style': 'position: absolute; left: 410px;',
                                                                 'placeholder': 'Password', }),
                                                      label="New Password: ")

        self.fields['new_password_confirm'] = forms.CharField(required=False,
                                                              widget=forms.PasswordInput(
                                                                  attrs={'class': 'inputfield',
                                                                         'style': 'position: absolute; left: 410px;',
                                                                         'placeholder': 'Password', }),
                                                              label="New Password 2: ")


class EditInterestForm(forms.Form):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        profile = Profile.objects.get(user=user)

        self.CHOICES = [(0, '---')]
        for category in Category.objects.all():
            self.CHOICES.append((category.id, category))

        self.LOCATIONS = [(0, '---')]
        for location in Location.objects.all():
            self.LOCATIONS.append((location.id, location))

        super(EditInterestForm, self).__init__(*args, **kwargs)
        self.fields['interests'] = forms.MultipleChoiceField(required=True,
                                                             widget=forms.Select(
                                                                 attrs={'class': 'inputfield selectfield',
                                                                        'style': 'position: absolute;'
                                                                                 'left: 410px;'
                                                                                 'background-color: rgb(178, 160, 157);'
                                                                                 'font-family: \'Poiret One\', cursive;'
                                                                                 'font-weight: bold;'
                                                                                 'font-size: 1.2em;'
                                                                        }, ),
                                                             label="Interests: ",
                                                             choices=self.CHOICES)

        self.fields['location'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                       'style': 'position: absolute;'
                                                                                'left: 410px;'
                                                                                'background-color: rgb(178, 160, 157);'
                                                                                'font-family: \'Poiret One\', cursive;'
                                                                                'font-weight: bold;'
                                                                                'font-size: 1.2em;'
                                                                       }, ),
                                                            label="Location: ",
                                                            choices=self.LOCATIONS)

    category = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])


class AddJobForm(forms.Form):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        self.CAT_CHOICES = []
        for category in Category.objects.all():
            self.CAT_CHOICES.append((category.id, category))

        self.LOC_CHOICES = []
        for location in Location.objects.all():
            self.LOC_CHOICES.append((location.id, location))

        super(AddJobForm, self).__init__(*args, **kwargs)
        self.fields['title'] = forms.CharField(required=True,
                                               widget=forms.TextInput(
                                                   attrs={'class': 'inputfield',
                                                          'style': 'position: absolute; left: 410px;', }),
                                               max_length=100,
                                               label="Title: ")

        self.fields['description'] = forms.CharField(required=True,
                                                     widget=forms.TextInput(
                                                         attrs={'class': 'inputfield',
                                                                'style': 'position: absolute; left: 410px;', }),
                                                     max_length=100,
                                                     label="Description: ")

        self.fields['category'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(

                                                                attrs={'class': 'inputfield selectfield',
                                                                       'style': 'position: absolute;'
                                                                                'left: 410px;'
                                                                                'background-color: rgb(178, 160, 157);'
                                                                                'font-family: \'Poiret One\', cursive;'
                                                                                'font-weight: bold;'
                                                                                'font-size: 1.2em;'}, ),
                                                            label="Category",
                                                            choices=self.CAT_CHOICES)

        self.fields['start_date'] = forms.DateField(required=True,

                                                    widget=forms.DateInput(
                                                        attrs={'class': 'inputfield datepicker',
                                                             'placeholder': 'dd/mm/yyyy',
                                                               'style': 'position: absolute; left: 410px;', }),
                                                    label="Start")

        self.fields['end_date'] = forms.DateField(required=True,

                                                  widget=forms.DateInput(
                                                      attrs={'class': 'inputfield datepicker',
                                                             'placeholder': 'dd/mm/yyyy',
                                                             'style': 'position: absolute; left: 410px;', }),
                                                  label="End")

        self.fields['number_of_hours'] = forms.CharField(required=True,
                                                         widget=forms.TextInput(
                                                             attrs={'class': 'inputfield',
                                                                    'style': 'position: absolute; left: 410px;', }),
                                                         label="Number of hours: ")

        self.fields['per_time'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={
                                                                    'class': 'inputfield selectfield',
                                                                    'style': 'position: absolute;'
                                                                             'left: 410px;'
                                                                             'background-color: rgb(178, 160, 157);'
                                                                             'font-family: \'Poiret One\', cursive;'
                                                                             'font-weight: bold;'
                                                                             'font-size: 1.2em;'}, ),
                                                            choices=Job.PER_TIME_CHOICES
                                                            )

        self.fields['cost_of_services'] = forms.CharField(required=False,
                                                          widget=forms.TextInput(
                                                              attrs={'class': 'inputfield',
                                                                     'style': 'position: absolute; left: 410px;', }),
                                                          label="Cost of services: ")

        self.fields['per_cost'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                       'style': 'position: absolute;'
                                                                                'left: 410px;'
                                                                                'background-color: rgb(178, 160, 157);'
                                                                                'font-family: \'Poiret One\', cursive;'
                                                                                'font-weight: bold;'
                                                                                'font-size: 1.2em;'}, ),
                                                            choices=Job.PER_COST_CHOICES
                                                            )

        self.fields['location'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                       'style': 'position: absolute;'
                                                                                'left: 410px;'
                                                                                'background-color: rgb(178, 160, 157);'
                                                                                'font-family: \'Poiret One\', cursive;'
                                                                                'font-weight: bold;'
                                                                                'font-size: 1.2em;'}, ),
                                                            label="Location",
                                                            choices=self.LOC_CHOICES)

        self.fields['requirements'] = forms.CharField(required=True,
                                                      widget=forms.TextInput(
                                                          attrs={'class': 'inputfield',
                                                                 'style': 'position: absolute; left: 410px;', }),
                                                      label="Requirements: ")

    category = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    location = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    per_time = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    per_cost = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])


class EditJobForm(forms.Form):
    def __init__(self, *args, **kwargs):
        job = kwargs.pop('job')
        self.CAT_CHOICES = []
        for category in Category.objects.all():
            self.CAT_CHOICES.append((category.id, category))

        self.LOC_CHOICES = []
        for location in Location.objects.all():
            self.LOC_CHOICES.append((location.id, location))

        super(EditJobForm, self).__init__(*args, **kwargs)
        self.fields['title'] = forms.CharField(required=True,
                                               widget=forms.TextInput(
                                                   attrs={'class': 'inputfield',
                                                          'style': 'position: absolute; left: 410px;', }),
                                               max_length=100,
                                               initial=job.name,
                                               disabled=True,
                                               label="Title: ")

        self.fields['description'] = forms.CharField(required=True,
                                                     widget=forms.TextInput(
                                                         attrs={'class': 'inputfield',
                                                                'style': 'position: absolute; left: 410px;', }),
                                                     max_length=100,
                                                     initial=job.description,
                                                     label="Description: ")

        self.fields['category'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(

                                                                attrs={'class': 'inputfield selectfield',
                                                                       'style': 'position: absolute;'
                                                                                'left: 410px;'
                                                                                'background-color: rgb(178, 160, 157);'
                                                                                'font-family: \'Poiret One\', cursive;'
                                                                                'font-weight: bold;'
                                                                                'font-size: 1.2em;'}, ),
                                                            label="Category",
                                                            initial=job.category,
                                                            disabled=True,
                                                            choices=self.CAT_CHOICES)

        self.fields['start_date'] = forms.DateField(required=True,

                                                    widget=forms.DateInput(
                                                        attrs={'class': 'inputfield datepicker',
                                                             'placeholder': 'dd/mm/yyyy',
                                                               'style': 'position: absolute; left: 410px;', }),
                                                    initial='/'.join(str(job.end_date).split('-')[::-1]),
                                                    label="Start")

        self.fields['end_date'] = forms.DateField(required=True,

                                                  widget=forms.DateInput(
                                                      attrs={'class': 'inputfield datepicker',
                                                             'placeholder': 'dd/mm/yyyy',
                                                             'style': 'position: absolute; left: 410px;', }),
                                                  initial='/'.join(str(job.end_date).split('-')[::-1]),
                                                  label="End")

        self.fields['number_of_hours'] = forms.CharField(required=True,
                                                         widget=forms.TextInput(
                                                             attrs={'class': 'inputfield',
                                                                    'style': 'position: absolute; left: 410px;', }),
                                                         initial=job.number_of_hours,
                                                         label="Number of hours: ")

        self.fields['per_time'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={
                                                                    'class': 'inputfield selectfield',
                                                                    'style': 'position: absolute;'
                                                                             'left: 410px;'
                                                                             'background-color: rgb(178, 160, 157);'
                                                                             'font-family: \'Poiret One\', cursive;'
                                                                             'font-weight: bold;'
                                                                             'font-size: 1.2em;'}, ),
                                                            initial=job.per_time,
                                                            choices=Job.PER_TIME_CHOICES
                                                            )

        self.fields['cost_of_services'] = forms.CharField(required=False,
                                                          widget=forms.TextInput(
                                                              attrs={'class': 'inputfield',
                                                                     'style': 'position: absolute; left: 410px;', }),
                                                          initial=job.cost_of_services,
                                                          label="Cost of services: ")

        self.fields['per_cost'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                       'style': 'position: absolute;'
                                                                                'left: 410px;'
                                                                                'background-color: rgb(178, 160, 157);'
                                                                                'font-family: \'Poiret One\', cursive;'
                                                                                'font-weight: bold;'
                                                                                'font-size: 1.2em;'}, ),
                                                            initial=job.per_cost,
                                                            choices=Job.PER_COST_CHOICES
                                                            )

        self.fields['location'] = forms.MultipleChoiceField(required=True,
                                                            widget=forms.Select(
                                                                attrs={'class': 'inputfield selectfield',
                                                                       'style': 'position: absolute;'
                                                                                'left: 410px;'
                                                                                'background-color: rgb(178, 160, 157);'
                                                                                'font-family: \'Poiret One\', cursive;'
                                                                                'font-weight: bold;'
                                                                                'font-size: 1.2em;'}, ),
                                                            label="Location",
                                                            initial=job.location,
                                                            choices=self.LOC_CHOICES)

        self.fields['requirements'] = forms.CharField(required=True,
                                                      widget=forms.TextInput(
                                                          attrs={'class': 'inputfield',
                                                                 'style': 'position: absolute; left: 410px;', }),
                                                      initial=job.requirements,
                                                      label="Requirements: ")

    category = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    location = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    per_time = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])
    per_cost = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])

class CategorySearchForm(forms.Form):
    def __init__(self, *args, **kwargs):

        self.CAT_CHOICES = []
        for category in Category.objects.all():
            self.CAT_CHOICES.append((category.id, category))

        super(CategorySearchForm, self).__init__(*args, **kwargs)
        self.fields['category'] = forms.MultipleChoiceField(required=True,
                                               widget=forms.Select(
                                                   attrs={'class': 'inputfield selectfield',
                                                         'style': 'width:15%; '
                                                                  'background-color: rgb(178, 160, 157); '
                                                                  'font-size: 1em', },),
                                               label="Category",
                                               choices=[('0', 'All categories')] + self.CAT_CHOICES)

    category = forms.MultipleChoiceField(required=True, widget=forms.Select(), choices=[])

class SearchByHoursForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SearchByHoursForm, self).__init__(*args, **kwargs)

        self.fields['min_hours'] = forms.IntegerField(required=False,
                                                      widget=forms.TextInput(
                                                          attrs={
                                                          }),
                                                      label="Minimum nr. hours: ")

        self.fields['max_hours'] = forms.IntegerField(required=False,
                                                      widget=forms.TextInput(
                                                          attrs={
                                                          }),
                                                      label="Maximum nr. hours: ")
        self.fields['per_time'] = forms.MultipleChoiceField(required=False,
                                                            widget=forms.Select(
                                                                attrs={
                                                                    'style': 'background-color: rgb(178, 160, 157); ', }, ),
                                                            choices=Job.PER_TIME_CHOICES
                                                            )

    per_time = forms.MultipleChoiceField(required=False, widget=forms.Select(), choices=[])


class ReviewForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)

        self.fields['stars'] = forms.ChoiceField(required=True,
                                                 widget=forms.Select(
                                                     attrs={'class': 'inputfield selectfield',
                                                            'style': 'left: 410px;'
                                                                     'background-color: rgb(178, 160, 157);'
                                                                     'font-family: \'Poiret One\', cursive;'
                                                                     'font-weight: bold;'
                                                                     'font-size: 1.2em;'}, ),
                                                 choices=Review.STAR_CHOICES
                                                 )
        self.fields['comment'] = forms.CharField(required=True,
                                                 widget=forms.TextInput(
                                                     attrs={'class': 'inputfield',
                                                            'style': 'left: 410px;'
                                                                     'background-color: rgb(178, 160, 157);'
                                                                     'font-family: \'Poiret One\', cursive;'
                                                                     'font-weight: bold;'
                                                                     'font-size: 1.2em;'}, ),
                                                 label="Comment: ")

    stars = forms.ChoiceField(required=True, widget=forms.Select(), choices=[])


class SearchByIntervalForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SearchByIntervalForm, self).__init__(*args, **kwargs)

        self.fields['start_date'] = forms.DateField(required=False,
                                                    widget=forms.DateTimeInput(
                                                        attrs={'class': 'datepicker',
                                                               'placeholder': 'dd/mm/yyyy', }),
                                                    label="Start")

        self.fields['end_date'] = forms.DateField(required=False,
                                                  widget=forms.DateTimeInput(
                                                      attrs={'class': 'datepicker',
                                                             'placeholder': 'dd/mm/yyyy', }),
                                                  label="End")


class SearchByCostForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SearchByCostForm, self).__init__(*args, **kwargs)

        self.fields['min_cost'] = forms.IntegerField(required=False,
                                                     widget=forms.TextInput(
                                                         attrs={'class': 'costField'
                                                                }),
                                                     label="Minimum cost: ")

        self.fields['max_cost'] = forms.IntegerField(required=False,
                                                     widget=forms.TextInput(
                                                         attrs={'class': 'costField'
                                                                }),
                                                     label="Maximum cost: ")
        self.fields['per_cost'] = forms.MultipleChoiceField(required=False,
                                                            widget=forms.Select(
                                                                attrs={'class': 'costField',
                                                                       'style': 'background-color: rgb(178, 160, 157); ', }, ),
                                                            choices=Job.PER_COST_CHOICES
                                                            )
        self.fields['voluntary'] = forms.BooleanField(required=False,
                                                      widget=forms.CheckboxInput(
                                                          attrs={
                                                              'id': 'voluntaryCB'}
                                                      ))