from django import template

register = template.Library()

@register.simple_tag
def inlist(elem, elem_list):
    return elem.id in [e.id for e in elem_list]