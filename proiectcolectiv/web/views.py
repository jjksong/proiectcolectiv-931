# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from itertools import zip_longest

from django.contrib.auth.models import User
from django.http import HttpResponse, Http404
from django.shortcuts import redirect, get_object_or_404
# Create your views here.
from django.template import loader

from .filter import *
from .forms import *
from .models import *
from django.utils.dateparse import *


def get_menu(request):
    context = {}
    template = loader.get_template('menu.html')
    return template.render(context, request)


def home(request):
    content = loader.get_template('home.html')
    content_context = {

    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def jobs(request):
    jobs_page = 1
    categories_list=[]
    min_hours=0
    max_hours=0
    per_time=''
    if 'page' in request.GET:
        jobs_page = request.GET['page']

    try:
        jobs_page = int(jobs_page)
    except ValueError:
        raise Http404("Job page could not be found")

    job_list = Job.objects.order_by('-id')
    job_list = filter_jobs(request, job_list)

    if 'category' in request.GET:
        categories_list = request.GET.getlist('category')


    #### FOR FILTERS WRITE CODE HERE
    '''
    Filter job_list in filter.py
    For example:
    job_list = filter_category(job_list) , where filter_category would be function in filter.py (does not exist yet)
    '''
    if '0' not in categories_list:
        for category in categories_list:
            try:
                category = int(category)
            except ValueError:
                raise Http404("Category could not be found")
            check = get_object_or_404(Category, pk=int(category))
        job_list=filter_category(job_list,categories_list)
    if "searchByHours" in request.GET and 'per_time' in request.GET:
        min_hours, max_hours = None, None
        if 'min_hours' in request.GET:
            min_hours = request.GET['min_hours']
        if 'max_hours' in request.GET:
            max_hours = request.GET['max_hours']
        per_time = request.GET['per_time']

        try:
            min_hours = int(min_hours)
            max_hours = int(max_hours)
            job_list = filter_jobs_hours(job_list, min_hours, max_hours, per_time)
        except ValueError:
            raise Http404("Job page could not be found")

    if "searchByCost" in request.GET:
        min_cost, max_cost, per_cost, voluntary = 0, 0, None, None
        if 'min_cost' in request.GET:
            min_cost = request.GET['min_cost']
        if 'max_cost' in request.GET:
            max_cost = request.GET['max_cost']
        if 'per_cost' in request.GET:
            per_cost = request.GET['per_cost']
        if 'voluntary' in request.GET:
            voluntary = request.GET['voluntary']
        for elem in request:
            print(elem.value)
        try:
            min_cost = int(min_cost)
            max_cost = int(max_cost)
            job_list = filter_jobs_cost(job_list, min_cost, max_cost, per_cost, voluntary)
        except ValueError:
            raise Http404("Job page could not be found")

    if "searchByInterval" in request.GET:
        start_date, end_date = None, None
        if 'start_date' in request.GET:
            start_date = request.GET['start_date']
        if 'end_date' in request.GET:
            end_date = request.GET['end_date']

        try:
            start_date = datetime.datetime.strptime(start_date, '%d/%m/%Y')
            end_date = datetime.datetime.strptime(end_date, '%d/%m/%Y')
            # print("START: "+str(start_date)+"\n")
            # print("END: "+str(end_date)+"\n")
            if start_date > end_date:
                raise ValueError
            job_list = filter_jobs_interval(job_list, start_date, end_date)
        except ValueError:
            raise Http404("Invalid date format")

    #### UNTIL HERE
    # Leave this as it is, as we need the paged view
    max_page, job_list, page_buttons = filter_content(job_list, jobs_page)
    if job_list is None:
        raise Http404("Job page could not be found")

    content = loader.get_template('jobs.html')
    paged = loader.get_template('paged.html')

    paged_context = {
        'page': jobs_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
    }

    content_context = {
        'jobs': job_list,
        'paged': paged.render(paged_context, request),
        'searchByHoursForm': SearchByHoursForm(),
        'searchByIntervalForm': SearchByIntervalForm(),
        'searchByCostForm': SearchByCostForm(),
        'category_form': CategorySearchForm(),
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)

    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def seekers(request):
    seekers_page = 1
    if 'page' in request.GET:
        seekers_page = request.GET['page']
    try:
        seekers_page = int(seekers_page)
    except ValueError:
        raise Http404("Users page could not be found")
    seekers_list = Profile.objects.order_by('-id')

    max_page, seekers_list, page_buttons = filter_content(seekers_list, seekers_page)
    if seekers_list is None:
        raise Http404("Users page could not be found")

    content = loader.get_template('seekers.html')
    paged = loader.get_template('paged.html')
    paged_context = {
        'page': seekers_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
    }

    for seeker in seekers_list:
        reviews = Review.objects.filter(user_reviewed=seeker.user, employer_review=False)
        if reviews.count() == 0:
            seeker.average_applicant_rating = None
        else:
            seeker.average_applicant_rating = sum([review.stars for review in reviews]) / reviews.count()

    content_context = {
        'seekers': seekers_list,
        'paged': paged.render(paged_context, request)
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def profile(request, username):
    if not request.user.is_authenticated:
        return redirect('/home')
    if username == '':
        return redirect('/profile/' + str(request.user.username))
    user = None
    try:
        user = User.objects.get(username=username)
    except:
        return redirect('/home')
    content = loader.get_template('profile.html')
    user_profile = Profile.objects.get(user=user)
    jobs_list = Job.objects.filter(user=user).order_by('-id')[:5]
    interests = Interest.objects.filter(profile=user_profile).order_by('-id')[:5]
    notifications = Notification.objects.filter(user=user_profile, is_read=False).order_by('-id')[:5]
    content_context = {
        'user': user,
        'profile': user_profile,
        'jobs': jobs_list,
        'interests': interests,
        'notifications': notifications,
    }
    if user == request.user:
        content_context['own_profile'] = 'True'
    content_context['zip'] = zip_longest(content_context['jobs'], content_context['interests'])
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def edit_profile(request):
    if not request.user.is_authenticated:
        redirect('/home')
    content = loader.get_template('edit.html')
    profile = Profile.objects.get(user=request.user)
    content_context = {
        'user': request.user,
        'profile': profile,
        'form': EditProfileForm(user=request.user),
        'interests_form': EditInterestForm(user=request.user),
        'location_id': profile.location.id if profile.location else 0
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def edit_my_jobs(request):
    if not request.user.is_authenticated:
        return redirect('/home')
    content = loader.get_template('my_jobs.html')
    user_profile = Profile.objects.get(user=request.user)
    jobs_list = Job.objects.filter(user=request.user).order_by('-id')
    content_context = {
        'user': request.user,
        'profile': user_profile,
        'jobs': jobs_list,
    }

    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def edit_interest_profile(request):
    if not request.user.is_authenticated:
        redirect('/home')
    content = loader.get_template('edit.html')
    content_context = {
        'user': request.user,
        'profile': Profile.objects.get(user=request.user),
        'form': EditInterestForm(user=request.user)
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def job_add(request):
    if not request.user.is_authenticated:
        redirect('/home')
    content = loader.get_template('job_add.html')
    content_context = {
        'form': AddJobForm(user=request.user)
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def job_edit(request, job_id):
    if not request.user.is_authenticated:
        redirect('/home')
    content = loader.get_template('job_edit.html')

    job = get_object_or_404(Job, pk=job_id)

    content_context = {
        'form': EditJobForm(job=job),
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def job_detail(request, job_id):
    job_id = int(job_id)
    job = get_object_or_404(Job, pk=job_id)
    applicants = [profile.user for profile in job.applicants.all()]
    if not job.is_available and request.user not in applicants and request.user != job.user:
        raise Http404("Job was not found!")

    seekers_page = 1
    if 'page' in request.GET:
        seekers_page = request.GET['page']
    try:
        seekers_page = int(seekers_page)
    except ValueError:
        raise Http404("Users page could not be found")
    applicants_list = job.applicants.all()

    max_page, applicants_list, page_buttons = filter_content(applicants_list, seekers_page)
    if applicants_list is None:
        raise Http404("Could not load applicants")

    paged = loader.get_template('paged.html')
    content = loader.get_template('job_detail.html')
    paged_context = {
        'page': seekers_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
    }
    content_context = {
        'job': job,
        'paged': paged.render(paged_context, request),
        'applicants': applicants,
        'reviewable': True if job.end_date < datetime.date.today() and not job.is_available and
                              Review.objects.filter(user_reviewing=request.user, job=job).count() == 0 and (
                                  (job.user == request.user) or
                                  (job.user_chosen and job.user_chosen.user == request.user)
                              ) else False,
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def job_choose_applicant(request, job_id):
    job_id = int(job_id)

    if "user" not in request.POST:
        raise Http404("User was not found")

    profile = get_object_or_404(Profile, pk=int(request.POST["user"]))
    job = get_object_or_404(Job, pk=job_id)

    job.user_chosen = profile
    job.is_available = False
    job.save()

    notification_message = "You have been selected for the job " + job.name
    Notification.objects.create(user=profile, job=job, receive_date=datetime.datetime.now(),
                                is_read=False, notification_text=notification_message)

    return redirect('/job/' + str(job_id))


def job_deselect_applicant(request, job_id):
    job_id = int(job_id)

    if "user" not in request.POST:
        raise Http404("User was not found")

    profile = get_object_or_404(Profile, pk=int(request.POST["user"]))
    job = get_object_or_404(Job, pk=job_id)

    job.user_chosen = None
    job.is_available = True
    job.save()

    notification_message =  "You have been de-selected from the job " + job.name
    Notification.objects.create(user=profile, job=job, receive_date=datetime.datetime.now(),
                                is_read=False, notification_text=notification_message)

    return redirect('/job/' + str(job_id))


def job_apply(request, job_id):
    job_id = int(job_id)

    if "user" not in request.POST:
        raise Http404("Could not apply for the job!")

    user = get_object_or_404(User, id=int(request.POST["user"]))
    profile = get_object_or_404(Profile, user=user)

    job = get_object_or_404(Job, pk=job_id)
    Application.objects.create(user=profile, job=job)

    owner_profile = get_object_or_404(Profile, user=job.user)
    applicant_name = profile.user.first_name + " " + profile.user.last_name if len(profile.user.first_name) > 0 and len(
        profile.user.last_name) > 0 else profile.user.username
    notification_message = applicant_name + " has applied for your job: " + job.name
    Notification.objects.create(user=owner_profile, job=job, receive_date=datetime.datetime.now(),
                                is_read=False, notification_text=notification_message)

    return redirect('/job/' + str(job_id))


def handle_notification(request, notification_id):
    notification = Notification.objects.get(id=notification_id)
    notification.is_read = True
    notification.save()
    return redirect('/job/' + str(notification.job.id))


def notifications(request):
    notifications_page = 1
    if 'page' in request.GET:
        notifications_page = request.GET['page']

    try:
        notifications_page = int(notifications_page)
    except ValueError:
        raise Http404("Notification page could not be found")

    user_profile = Profile.objects.get(user=request.user)
    notification_list = Notification.objects.filter(user=user_profile).order_by('-id')
    max_page, notification_list, page_buttons = filter_content(notification_list, notifications_page)
    if notification_list is None:
        raise Http404("Notification page could not be found")

    content = loader.get_template('notifications.html')
    paged = loader.get_template('paged.html')

    paged_context = {
        'page': notifications_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
    }

    content_context = {
        'notifications': notification_list,
        'paged': paged.render(paged_context, request)
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def job_review(request, job_id):
    job = get_object_or_404(Job, pk=job_id)

    content = loader.get_template('review.html')
    content_context = {
        'job': job,
        'user': request.user,
        'form': ReviewForm(),
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))


def reviews(request, username):
    reviews_page = 1
    if 'page' in request.GET:
        reviews_page = request.GET['page']

    try:
        reviews_page = int(reviews_page)
    except ValueError:
        raise Http404("Review page could not be found!")

    user = get_object_or_404(User, username=username)

    review_list = Review.objects.filter(user_reviewed=user).order_by('-id')
    max_page, review_list, page_buttons = filter_content(review_list, reviews_page)
    if review_list is None:
        raise Http404("Notification page could not be found")

    content = loader.get_template('reviews.html')
    paged = loader.get_template('paged.html')

    paged_context = {
        'page': reviews_page,
        'max_page': max_page,
        'page_buttons': page_buttons,
    }

    content_context = {
        'reviews': review_list,
        'paged': paged.render(paged_context, request)
    }
    context = {
        'menu': get_menu(request),
        'content': content.render(content_context, request)
    }
    template = loader.get_template('template.html')
    return HttpResponse(template.render(context, request))
