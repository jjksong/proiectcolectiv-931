import re
from django.contrib.auth import logout, authenticate, login, update_session_auth_hash
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import redirect
from .models import Profile, Category, Location, Interest

# ----------------------------
#      Register / Login      |
# ----------------------------


def log_out(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect('/')


def auth_ajax(request):

    if request.user.is_authenticated:
        logout(request)
    if 'username' in request.POST and 'password' in request.POST:
        if request.POST['username'] == '':
            return JsonResponse({'msg': 'Empty username.'})
        if request.POST['password'] == '':
            return JsonResponse({'msg': 'Empty password.'})
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            profile, created = Profile.objects.get_or_create(user=user)
            if created:
                profile.save()
            return JsonResponse({'msg': 'success'})
        else:
            return JsonResponse({'msg': 'Invalid username or password'})
    return JsonResponse({'msg': 'Missing username or password'})


def check_checkbox(request):
    if 'interest' not in request.POST:
        return {'sddsdsedsad'}


def check_empty_fields(request):
    if 'username' not in request.POST:
        return {'reg-user-err': 'Please enter a username'}
    if 'email' not in request.POST:
        return {'reg-email-error': 'Please enter an e-mail address'}
    if 'password' not in request.POST:
        return {'reg-pass-error': 'Please enter a password'}
    if 'confirm-password' not in request.POST:
        return {'reg-confirm-error': 'Please confirm password'}
    return None


def validate_username(request):
    if request.POST['username'] == '':
        return {'reg-user-error': 'Please enter a username'}
    username = request.POST['username']
    if len(username) < 8:
        return {'reg-user-error': 'Username must contain at least 8 characters.'}
    if len(username) > 20:
        return {'reg-user-error': 'Username must contain at most 20 characters.'}
    if not re.match(r'^[A-Za-z].*', username):
        return {'reg-user-error': 'Username does not start with a letter'}
    if User.objects.filter(username=username).exists():
        return {'reg-user-error': 'Username is taken.'}
    if not re.match(r'^\w+$', username):
        return {'reg-user-error': 'Username can only contain letters, numbers and the underscore'}
    return None


def validate_email(request):
    error_key = 'reg-email-error'
    if request.POST['email'] == '':
        return {error_key: 'Please enter an e-mail address.'}
    email = request.POST['email']
    if len(email) > 254:
        return {error_key: 'E-mail address is too long'}
    if re.search(r"[^@]+@[^@]+\.[a-zA-Z]+", email) is None:
        return {error_key: 'E-mail address looks invalid'}
    return None


def validate_password(request):
    password = request.POST['password']
    error_key = 'reg-pass-error'
    if request.POST['password'] == '':
        return {error_key: 'Please enter a password'}

    if len(password) < 8:
        return {error_key: 'Password too short.'}
    if len(password) > 35:
        return {error_key: 'Password too long. How will you remember that?'}
    if not re.match(r'[A-Za-z0-9@#$%^&+=()!_*{}:;/".,?~`<>| \-\'\[\]\\]{8,35}', password):
        return {error_key: 'Password contains invalid characters.'}
    if not re.search(r'[A-Za-z]+', password):
        return {error_key: 'Password does not contain letters'}
    if not re.search(r'[0-9]+', password):
        return {error_key: 'Password does not contain digits'}
    if not re.search(r'[^A-Za-z0-9]+', password):
        return {error_key: 'Password does not contain symbols'}
    if password == request.POST['email']:
        return {error_key: 'Password cannot match email address'}
    if password == request.POST['username']:
        return {error_key: 'Password cannot match username'}
    return None


def validate_confirm_password(request):
    if request.POST['confirm-password'] == '':
        return {'reg-confirm-error': 'Please confirm password'}
    confirm_password = request.POST['confirm-password']
    if len(confirm_password) != len(request.POST['password']) or confirm_password != request.POST['password']:
        return {'reg-confirm-error': 'Passwords do not match!'}
    return None


# --------------------------------
#          Edit profile          |
# --------------------------------

def check_edit_fields(request):
    if request.POST['first_name'] == '' and request.POST['last_name'] == '' and request.POST['email'] == '':
        return {'edit-firstname-error': 'Please enter at least your first name'}
    return None


def edit_profile_validate_password(request):
    password = request.POST['new_password']
    error_key = 'edit-pass-error'
    if request.POST['new_password'] == '':
        return {error_key: 'Please enter a password'}

    if len(password) < 8:
        return {error_key: 'Password too short.'}
    if len(password) > 35:
        return {error_key: 'Password too long. How will you remember that?'}
    if not re.match(r'[A-Za-z0-9@#$%^&+=()!_*{}:;/".,?~`<>| \-\'\[\]\\]{8,35}', password):
        return {error_key: 'Password contains invalid characters.'}
    if not re.search(r'[A-Za-z]+', password):
        return {error_key: 'Password does not contain letters'}
    if not re.search(r'[0-9]+', password):
        return {error_key: 'Password does not contain digits'}
    if not re.search(r'[^A-Za-z0-9]+', password):
        return {error_key: 'Password does not contain symbols'}
    if password == request.POST['email']:
        return {error_key: 'Password cannot match email address'}
    if password == request.POST['old_password']:
        return {error_key: 'The new password cannot match the old one'}
    return None


def edit_profile_validate_email(request):
    error_key = 'edit-email-error'
    if request.POST['email'] == '':
        return None
    email = request.POST['email']
    if len(email) > 254:
        return {error_key: 'E-mail address is too long'}
    if re.search(r"[^@]+@[^@]+\.[a-zA-Z]+", email) is None:
        return {error_key: 'E-mail address looks invalid'}
    return None


def edit_profile_confirm_password(request):
    if request.POST['new_password_confirm'] == '':
        return {'edit-pass-confirm-error': 'Please confirm password'}
    confirm_password = request.POST['new_password_confirm']
    if len(confirm_password) != len(request.POST['new_password']) or confirm_password != request.POST['new_password']:
        return {'edit-pass-confirm-error': 'Passwords do not match!'}
    return None


def validate_interests(request):
    if 'interests' not in request.POST or len(request.POST['interests']) == 0 or request.POST['interests'] == '0':
        return {'edit-interests-error': 'Please select an interest'}
    return None


def edit_ajax(request):
    err = False
    user = request.user
    errors = {}
    empty_check = check_edit_fields(request)
    if empty_check is not None:
        return JsonResponse(empty_check)
    if 'new_password' in request.POST and request.POST['new_password'] != '':
        password_check = edit_profile_validate_password(request)
        if password_check is not None:
            err = True
            for key in password_check:
                errors[key] = password_check[key]
        confirm_check = edit_profile_confirm_password(request)
        if confirm_check is not None:
            err = True
            for key in confirm_check:
                errors[key] = confirm_check[key]

    email_check = edit_profile_validate_email(request)
    if email_check is not None:
        err = True
        for key in email_check:
            errors[key] = email_check[key]

    if not user.check_password(request.POST['old_password']):
        err = True
        errors['edit-confirm-error'] = 'Incorrect password!'

    if not err:
        new_passwd = request.POST['new_password']
        if new_passwd is not None and new_passwd != '':
            user.set_password(request.POST['new_password'])
            update_session_auth_hash(request, request.user)
        user.first_name = request.POST['first_name']
        user.last_name = request.POST['last_name']
        user.email = request.POST['email']
        if 'location' in request.POST and request.POST['location'] != '0':
            profile = Profile.objects.get(user=user)
            profile.location = Location.objects.get(pk=int(request.POST['location']))
            profile.save()
        user.save()

        return JsonResponse({'': ''})
    else:
        return JsonResponse(errors)


def edit_interest_ajax(request):
    user = request.user
    errors = validate_interests(request)

    if not errors:
        profile = Profile.objects.get(user=user)
        if request.POST['interests'] != '0':
            category_object = Category.objects.get(pk=int(request.POST['interests']))
            interest_object, created = Interest.objects.get_or_create(category=category_object, profile=profile)
            interest_object.pay = True if request.POST['pay-interest'] == 'false' else False
            interest_object.save()
        return JsonResponse({'': ''})
    else:
        return JsonResponse(errors)


def delete_interest_profile(request):
    print(request.POST)
    profile = Profile.objects.get(user=request.user)
    category_object = Category.objects.get(pk=int(request.POST['interest']))
    interest = Interest.objects.get(category=category_object, profile=profile)
    interest.delete()
    return redirect('/edit')


def reg_ajax(request):
    err = False
    errors = {}
    empty_check = check_empty_fields(request)
    if empty_check is not None:
        return empty_check

    username_check = validate_username(request)
    if username_check is not None:
        err = True
        for key in username_check:
            errors[key] = username_check[key]

    email_check = validate_email(request)
    if email_check is not None:
        err = True
        for key in email_check:
            errors[key] = email_check[key]

    password_check = validate_password(request)
    if password_check is not None:
        err = True
        for key in password_check:
            errors[key] = password_check[key]

    confirm_check = validate_confirm_password(request)
    if confirm_check is not None:
        err = True
        for key in confirm_check:
            errors[key] = confirm_check[key]

    if not err:
        user = User.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password'])
        user.save()
        profile, created = Profile.objects.get_or_create(user=user)
        if created:
            profile.save()
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
        return JsonResponse({'': ''})
    else:
        return JsonResponse(errors)


def edit_checks_ajax(request):
    profile = Profile.objects.get(user = request.user)

    if 'is_offering' in request.POST and request.POST['is_offering'] == '':
        profile.is_offering = not profile.is_offering
        profile.save()
        return JsonResponse({'': ''})
    if 'is_searching' in request.POST and request.POST['is_searching'] == '':
        profile.is_searching = not profile.is_searching
        profile.save()
        return JsonResponse({'': ''})

    #empty_check = check_checkbox(request)
    #if empty_check is not None:
     #   return empty_check
