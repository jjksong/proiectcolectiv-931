$(document).ready(function () {
    $('.seen-checkbox').each(function(){
        $(this).click(function(){
            var notification_id = $(this).val();
            var handle_url = '/handle_notification/' + notification_id;
            $(this).attr("disabled", true);
            $.ajax({
                url: handle_url,
                type: 'GET'
            })
        });
    });
});