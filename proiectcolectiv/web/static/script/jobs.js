$(document).ready(function () {
    displayHours();
    displayInterval();
    displayCost();

    function displayHours() {
        if ($('#searchByHours').is(":checked"))
            $("#searchByHoursForm").show();
        else
            $("#searchByHoursForm").hide();
    }

    $('#searchByHours').change(displayHours);
    
    function displayCost() {
        if ($('#searchByCost').is(":checked"))
            $("#searchByCostForm").show();
        else
            $("#searchByCostForm").hide();
    }

    $('#searchByCost').change(displayCost);

    function visibilityCostFields(){
        if ($('#voluntaryCB').is(":checked")) {
            $(".costField").prop('disabled', true);
            $("input.costField").each(function(){
                $(this).val(0);
            });
        }
        else
            $(".costField").prop('disabled', false);
            $("input.costField").each(function(){
                    $(this).val("");
                });
    }

    $('#voluntaryCB').change(visibilityCostFields);

    function displayInterval() {
        if ($('#searchByInterval').is(":checked"))
            $("#searchByIntervalForm").show();
        else
            $("#searchByIntervalForm").hide();
    }

    $('#searchByInterval').change(displayInterval);
    
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "2017:2030",
        dateFormat: "dd/mm/yy"

    });

});


