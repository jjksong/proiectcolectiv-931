$(document).ready(function () {
    $('#sendReview').click(function() {
        var data = {};
        $('#rating-error').text(String.fromCharCode(8192));
        $('#reviewForm input').each(function() {
            if ($(this).hasClass('inputfield')) {
                data[$(this).attr('name')] = $(this).val();
            }
            else if($(this).attr('name') === 'csrfmiddlewaretoken') {
                data[$(this).attr('name')] = $(this).val();
            }

        });
        $('#reviewForm select').each(function() {
            if ($(this).hasClass('selectfield')) {
               data[$(this).attr('name')] = $(this).find(':selected').val();
            }
        });
        var job_id = data["job_id"];
        console.log(data);
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/job/review/send',
            data: data,
            success: function(data) {
                if (data['msg'] === 'success') {
                    window.location.href = '/job/' + job_id;
                }
                else {
                    FlashService.error('review-error', data['msg']);
                }
            },
            error: function() {
                FlashService.error('review-error', 'A problem occurred on the server.');
            }
        });
    });
});