$(document).ready(function () {
    $("#edit_button").click(function () {
        window.location.href = '/edit'
    });

    $("#edit_jobs_button").click(function () {
        window.location.href = '/my_jobs'
    });

    update_height();

    $('.seen-checkbox').each(function(){
        $(this).click(function(){
            var notification_id = $(this).val();
            var handle_url = '/handle_notification/' + notification_id;
            var tr = $(this).closest('tr');
            var table = $("#notifications");
            tr.remove();
            if (table[0].rows.length == 1){
                table.remove();
                $('#new-notification-label').remove();
            }
            $.ajax({
                url: handle_url,
                type: 'GET',
                success: function(){
                    update_height();
                }
            });
        });
    });
});

function update_height(){
    var height = $("#notifications").height();
    if (height === undefined){
        var offersHeight = $(".offers-interests").height();
        if (offersHeight === undefined)
            $(".form").height(375);
        else
            $(".form").height(Math.max(375, offersHeight + 100));
    }
    else{
        $(".form").height(height + 500);
    }
}
