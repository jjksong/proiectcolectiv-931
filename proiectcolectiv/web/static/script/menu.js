$(document).ready(function () {
    $("#login-prompt").click(function () {
        $("#login-page").css("width", "100%");
        $("#navigation-list").addClass("blurred");
        $("#content-wrapper").addClass("blurred");
        $("#login-page").addClass("opened");
        $("#auth-focus").focus();
    });

    $("#close-login-prompt").click(function () {
        $("#login-page").css("width", "0%");
        $("#navigation-list").removeClass("blurred");
        $("#content-wrapper").removeClass("blurred");
        $("#login-page").removeClass("opened");
    });

    $("#register-prompt").click(function () {
        $("#register-page").css("width", "100%");
        $("#navigation-list").addClass("blurred");
        $("#content-wrapper").addClass("blurred");
        $("#register-page").addClass("opened");
        $("#reg-focus").focus();
    });

    $("#close-register-prompt").click(function () {
        $("#register-page").css("width", "0%");
        $("#navigation-list").removeClass("blurred");
        $("#content-wrapper").removeClass("blurred");
        $("#register-page").removeClass("opened");
    });

    $("#logout-prompt").click(function () {
        window.location.href = "/logout";
    });

    $(document).keyup(function(e){
        if (e.which == 27){
            $("#close-register-prompt").click();
            $("#close-login-prompt").click();
        }
        else if (e.which == 13){
            if ($("#login-page").hasClass("opened"))
                $("#auth-button").click();
            if ($("#register-page").hasClass("opened"))
                $("#reg-button").click();
        }
    });
});