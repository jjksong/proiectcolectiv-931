from django.conf.urls import url

from .job import *
from .views import *
from .account import *




urlpatterns = [


    #Account-related
    url(r'^logout$', log_out, name='log out'),
    url(r'^account/edit$', edit_ajax, name='edit profile ajax'),
    url(r'^account/edit/addinterest$', edit_interest_ajax, name='edit interest ajax'),
    url(r'^account/edit/deleteinterest$', delete_interest_profile, name='delete interest profile'),
    url(r'^account/edit/checks$', edit_checks_ajax, name='edit checks ajax'),
    url(r'^account/login$', auth_ajax, name='authenticate'),
    url(r'^account/register$', reg_ajax, name='register'),

    #Profile-related
    url(r'^profile/(?P<username>[^\/]*)$', profile, name='view profile'),
    url(r'^profile/(?P<username>.*)/reviews$', reviews, name='user reviews'),
    url(r'^edit$', edit_profile, name='edit profile'),
    url(r'^edit/addinterest$', edit_interest_profile, name='edit interest profile'),
    url(r'^seekers$', seekers, name='view job seekers'),
    url(r'^handle_notification/(?P<notification_id>.*)$', handle_notification, name='handle notification'),
    url(r'^notifications$', notifications, name='notifications'),

    #Job-related
    url(r'^jobs$', jobs, name='view jobs'),
    url(r'^job/add$', add_ajax, name='add job ajax'),
    url(r'^job/edit$', edit_job_ajax, name='edit job ajax'),
    url(r'^job_add$', job_add, name='job add'),
    url(r'^job/(?P<job_id>[0-9]+)$', job_detail, name='job detail'),
    url(r'^job/(?P<job_id>[0-9]+)/edit$', job_edit, name='job edit'),
    url(r'^job/(?P<job_id>[0-9]+)/delete$', job_delete, name='job delete'),
    url(r'^job/(?P<job_id>[0-9]+)/apply$', job_apply, name='choose apply'),
    url(r'^job/(?P<job_id>[0-9]+)/choose$', job_choose_applicant, name='choose applicant'),
    url(r'^job/(?P<job_id>[0-9]+)/deselect$', job_deselect_applicant, name='deselect applicant'),

    #Profile-related
    url(r'^profile/(?P<username>.*)$', profile, name='view profile'),
    url(r'^edit$', edit_profile, name='edit profile'),
    url(r'^my_jobs$', edit_my_jobs, name='edit my jobs'),
    url(r'^edit/addinterest$', edit_interest_profile, name='edit interest profile'),
    url(r'^seekers$', seekers, name='view job seekers'),
    url(r'^handle_notification/(?P<notification_id>.*)$', handle_notification, name='handle notification'),
    url(r'^notifications$', notifications, name='notifications'),
    url(r'^job/(?P<job_id>[0-9]+)/review$', job_review, name='job review'),
    url(r'^job/review/send$', job_review_send_ajax, name='job review send'),

    #Keep this last
    url(r'$', home),
]