# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from django.db.models import CASCADE, DecimalField, FloatField, DateField
from django.db.models import ForeignKey
from django.db.models import IntegerField, CharField, DateTimeField, BooleanField, OneToOneField, ManyToManyField
import django.forms


class Category(models.Model):
    name = CharField(max_length=100)
    subscribers=ManyToManyField('Profile', through='Interest')

    @classmethod
    def create(cls, name):
        category = cls(name=name)
        return category

    def __str__(self):
        return self.name


class Location(models.Model):
    name = CharField(max_length=50)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = OneToOneField(User, on_delete=CASCADE)
    is_offering = BooleanField(default=False)
    is_searching = BooleanField(default=False)
    hide_email_for_strangers = BooleanField(default=False)
    interests = ManyToManyField(Category, through='Interest')
    notifications = ManyToManyField('Job', through='Notification')
    location = ForeignKey(Location, default=None, null=True)

    def __str__(self):
        return str(self.user.username)


class Job(models.Model):
    PER_TIME_CHOICES = (('d','day'), ('m','month'),('w','week'),('t','total'),)
    PER_COST_CHOICES = (('h','hour'), ('d','day'), ('m','month'),('w','week'),('t','total'),)
    name = CharField(max_length=100)
    description = CharField(max_length=500)
    user = ForeignKey(User, on_delete=CASCADE)

    publish_date = DateTimeField(auto_now_add=True, null=True)
    start_date = DateField(null=True)
    end_date = DateField(null=True)

    location = ForeignKey(Location)

    number_of_hours = IntegerField()
    per_time = CharField(max_length=10, choices=PER_TIME_CHOICES)

    cost_of_services = FloatField()
    per_cost = CharField(max_length=10, choices=PER_COST_CHOICES)

    requirements = CharField(default='', max_length=500)
    category = ForeignKey(Category, on_delete=CASCADE)

    applicants = ManyToManyField(Profile, through='Application')
    user_chosen = ForeignKey(Profile, related_name='user_chosen', null=True)

    is_available = BooleanField(default=True)
    average_applicant_rating = FloatField(null=True)
    average_employer_rating = FloatField(null=True)

    @classmethod
    def create(cls, name, description, user,
               publish_date, start_date, end_date,
               location, number_of_hours, per_time, cost_of_services, per_cost,
               requirements, category):
        job = cls(name=name, description=description, user=user,
                  publish_date=publish_date, start_date=start_date, end_date=end_date,
                  location=location, number_of_hours=number_of_hours, per_time=per_time,
                  cost_of_services=cost_of_services, per_cost=per_cost,
                  requirements=requirements, category=category)
        return job

    def __str__(self):
        return 'ID: ' + str(self.id) + ' ' + str(self.name)


class Notification(models.Model):
    user = ForeignKey(Profile)
    job = ForeignKey(Job)
    receive_date = DateTimeField(auto_now_add=True, null=True)
    is_read = BooleanField(default=False)
    notification_text = CharField(max_length=500)

    def __str__(self):
        return ('Unread' if not self.is_read else 'Read') + ' :' + str(self.notification_text)


class Application(models.Model):
    user = ForeignKey(Profile)
    job = ForeignKey(Job)
    apply_date = DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.user.user.username + '\'s application for ' + self.job.user.username + '\'s job named ' + self.job.name


class Interest(models.Model):
    pay = BooleanField(default=False)
    category = ForeignKey(Category)
    profile = ForeignKey(Profile)

    @classmethod
    def create(cls, pay, category, profile):
        interest = cls(pay=pay, category=category, profile=profile)
        return interest

    def __str__(self):
        return self.profile.user.username + '\'s ' + ('voluntary' if not self.pay else 'paid') + ' interest in ' + self.category.name

class Review(models.Model):
    STAR_CHOICES = (
        (0, '---'),
        (1, '*'),
        (2, '**'),
        (3, '***'),
        (4, '****'),
        (5, '*****')
    )

    user_reviewed = ForeignKey(User, related_name='reviewed')
    user_reviewing = ForeignKey(User, related_name='reviewing')
    job = ForeignKey(Job)
    stars = IntegerField(choices=STAR_CHOICES)
    comment = CharField(max_length=300)
    employer_review = BooleanField(default=False)
    timestamp = DateTimeField(auto_now_add=True, null=True)

    @classmethod
    def create(cls, user_reviewing, user_reviewed, job, stars, timestamp, comment, employer_review):
        review = cls(user_reviewing=user_reviewing, user_reviewed=user_reviewed, job=job, stars=stars, timestamp=timestamp, comment=comment, employer_review=employer_review)
        return review

    def __str__(self):
        return self.user_reviewing.username + '\'s review on ' + self.user_reviewed.username + ' as an ' + ('applicant' if not self.employer_review else 'employer') + ' is ' + str(self.stars) + ' stars'

